Game overview


Santa’s Deliveroo ​is a real time strategy game developed in Unity where the player has to take various santas to collect gifts and deliver them in the houses of the district, while avoiding the patrolling Befanas.

More in detail, t​he player will control multiple “Santa”​ units that fly above the city: units can be selected, and they can be instructed to ​collect gifts,​ ​deliver gifts​ to a house or to just r​ each a particular destination​.
Around the city there are also m​ultiple “Befana” units patrolling​: if a Santa comes into the detection range of a Befana, the Befana will chase him and upon reaching him ​the Befana will kidnap the Santa​ (obviously to take him to Honolulu!). 
Gifts that were collected by that Santa will be lost forever along with the Santa & Befana new couple.

Ho cercato di rendere il codice il più modulare e riusabile possibile, potete usarlo a vostra discrezione tanto più che ho finalmente l’occasione di creare un repository pubblico. 
L’architettura prevede queste classi:

GameManager - a singleton containing the main data of the game. It is maninly focused on camera and levels. It contains the main parameters of the game, such as, the number of object to instantiate and the various increments associated to the levels. It survives Scene Change. It is a fsm that switches from free camera and tactical camera (more on this later)

Level Manager - a singleton focused on automatically generate the levels wich instantiate the various Santa, Befana gifts and trees. It contains the current  level data and it contains an hidden renderer object that is needed to specify the game area

UiManager - singleton that handles the UI

SoundManager singleton that handles music and sond fx


FreeCameraMove - handles wasd /qe/ mouse movements when in free camera mode

PanoramicCamera - handles all the games action through a fsm when in tactical mode  

ZoomCamera - just used to show a fluid movement switching from free camera to tactical camera and back

SelectableObject - handle Santa entities  

MovingBot - handles Befana entities

Recipient  - handles House entities

CatchableObject - handles Present entities


