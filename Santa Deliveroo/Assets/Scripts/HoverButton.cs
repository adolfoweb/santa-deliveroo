﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HoverButton : Button
{

    UnityEvent mOnExit = new UnityEvent();
    UnityEvent mOnEnter = new UnityEvent();


    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        mOnExit.Invoke();
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        mOnEnter.Invoke();
    }

    public UnityEvent onExit
    {
        get { return mOnExit; }
    }

    public UnityEvent onEnter
    {
        get { return mOnEnter; }
    }
}
