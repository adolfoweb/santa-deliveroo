﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Recipient : MonoBehaviour
{
    public int targetIndex;
    public List<CatchableObject> catchedObjects;
    public Color color;
    public Color startColor;
    public Renderer objectToHighlight;
    public int numberToCatch;
    // Start is called before the first frame update
    void Start()
    {
        startColor = objectToHighlight.material.color;
        catchedObjects = new List<CatchableObject>();
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < catchedObjects.Count; i++)
        {
            catchedObjects[i].Follow(transform.position - transform.forward * 3);
        }
    }

    public void AddCatchedObject(CatchableObject obj)
    {
        obj.GetComponent<Collider>().enabled = false;
        obj.objectToFollow = gameObject;
        catchedObjects.Add(obj);
        if (catchedObjects.Count == numberToCatch) objectToHighlight.GetComponent<Collider>().enabled = false;
    }


    private void OnTriggerEnter(Collider other)
    {
        CatchableObject catchableObject = other.GetComponent<CatchableObject>();
        SelectableObject selectableObject = other.GetComponent<SelectableObject>();
        if (selectableObject)
        {
            foreach (CatchableObject c in selectableObject.catchedObjects.ToList())
            {
                if (c.target == this)
                {
                    selectableObject.RemoveCatchedObject(c);
                    AddCatchedObject(c);
                    GameManager.Instance.IncreaseDelivered();
                }
            }
        }

    }

    internal void RemoveHighlight()
    {
        objectToHighlight.material.color = startColor;
    }

    internal void Highlight(Color color)
    {
        objectToHighlight.material.color = color;


    }
    private void OnMouseEnter()
    {
        Highlight(color);
        foreach (CatchableObject c in FindObjectsOfType<CatchableObject>())
        {
            if (c.target == this)
            {
                c.Highlight(color);
            }
        }
    }

    private void OnMouseExit()
    {
        RemoveHighlight();
        foreach (CatchableObject c in FindObjectsOfType<CatchableObject>())
        {
            if (c.target == this)
            {
                c.RemoveHighlight();
            }
        }
    }
}