﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBot : MonoBehaviour
{

    Renderer[] rend;
    float frac;
    public float speed;
    int nextPointIndex;
    public List<Vector3> pathPoints;
    bool selected;
    public SelectableObject followingObject;
    public SelectableObject catchedObject;
    int currentIndex;
    public GameObject catchedObjectPlace;
    
    
    private float targetDistance;


    void Start()
    {
        speed = GameManager.Instance.befanasSpeed;
        rend = FindObjectsOfType<Renderer>();
        RemoveHighlight();
        
    }

    public void StartPath()
    {
        frac = 0;
        currentIndex = 0;
        nextPointIndex = 1;
        transform.position = pathPoints[currentIndex];
        transform.LookAt(pathPoints[nextPointIndex]);
        targetDistance = Vector3.Distance(transform.position, pathPoints[nextPointIndex]);
    }

    private void Update()
    {
        if (catchedObject)
        {
            Follow(GameManager.Instance.outOfScreen.transform.position, 0.01f*speed);
            catchedObject.Follow(catchedObjectPlace.transform.position);
            catchedObject.transform.LookAt(transform.position);
        }
        else 
        if (followingObject)
        {
            if (Vector3.Distance(transform.position, followingObject.transform.position) < GameManager.Instance.levelManager.befanaFollowingDistance)
            {
                Follow(followingObject.transform.position,speed*0.7f);
            }
            else
            {
                followingObject = null;
            }
        }
        else {
            
            frac += Time.deltaTime * speed/targetDistance;
            if (frac>=1)
            {
                frac = 0;
                currentIndex = nextPointIndex;
                nextPointIndex = (nextPointIndex + 1) % pathPoints.Count;
                targetDistance = Vector3.Distance(pathPoints[currentIndex], pathPoints[nextPointIndex]);
                transform.LookAt(pathPoints[nextPointIndex]);
            }
            else
            {
                transform.position = Vector3.Lerp(pathPoints[currentIndex], pathPoints[nextPointIndex], frac);
                CheckArea();
            }
        }
    }

    private void CheckArea()
    {
        foreach (SelectableObject obj in FindObjectsOfType<SelectableObject>())
        {
            if (Vector3.Distance(transform.position, obj.transform.position) < GameManager.Instance.levelManager.befanaFollowingDistance)
            {
                followingObject = obj;
            }
        }
    }

    public void Catch(SelectableObject obj)
    {
        SoundManager.Instance.PlayBefana();
        if (followingObject) followingObject.Highlight(Color.black);
        followingObject = null;
        Highlight(Color.black);
        obj.Caught();
        obj.transform.LookAt(transform.position);
        catchedObject = obj;
        pathPoints.Clear();
        Moveto( GameManager.Instance.outOfScreen);
    }

    void OnMouseOver()
    {
        Highlight(Color.black);
    }


    void OnMouseExit()
    {
        RemoveHighlight();
    }

    void Highlight(Color color)
    {
        foreach (Outline outline in transform.GetComponentsInChildren<Outline>())
        {

            outline.OutlineMode = Outline.Mode.OutlineAll;
            outline.OutlineColor = color;
            outline.OutlineWidth = 4f;
        }

    }

    void RemoveHighlight()
    {
        foreach (Outline outline in transform.GetComponentsInChildren<Outline>())
        {

            outline.OutlineMode = Outline.Mode.OutlineHidden;
            outline.OutlineColor = Color.clear;
        }
    }

    public void Moveto(GameObject point)
    {
        pathPoints.Add(transform.position);
        pathPoints.Add(point.transform.position);
        StartPath();
    }

    private void OnTriggerEnter(Collider other)
    {
        SelectableObject selectableObject = other.gameObject.GetComponent<SelectableObject>();
        if (selectableObject)
        {
            Catch(selectableObject);
        }
    }


    private void OnTriggerExit(Collider other)
    {

        followingObject = null;
        targetDistance = Vector3.Distance(transform.position, pathPoints[nextPointIndex]);
        transform.LookAt(pathPoints[nextPointIndex]);

    }

    public void Follow(Vector3 position,float speed)
    {
        transform.position = Vector3.Lerp(transform.position, position,speed * Time.deltaTime);
    }
}