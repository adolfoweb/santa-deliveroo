﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour
{
    private static SoundManager _instance;
    public static SoundManager Instance
    {
        get { return _instance; }
    }
    public AudioClip menuSound;
    public AudioClip startMusic;
    public AudioClip gameMusic;
    public AudioClip befana;
    public AudioClip delivered;
    public AudioSource generalAudio;
    public AudioSource music;

    private void Awake()
    {

        _instance = this;
    }

    private void Start()
    {
        
    }

    public void PlayMenuSound()
    {
        generalAudio.clip = menuSound;
        generalAudio.Play();
    }

    public void PlayStartMusic()
    {
        music.clip = startMusic;
        music.Play();
    }

    public void PlayGameMusic()
    {
        music.clip = gameMusic;
        music.Play();
    }

    public void StopMusic()
    {
        music.Stop();
    }

    internal void PlayDelivered()
    {
        generalAudio.clip = delivered;
        generalAudio.Play();
    }

    internal void PlayBefana()
    {
        generalAudio.clip = befana;
        generalAudio.Play();
    }
}