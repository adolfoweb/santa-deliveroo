﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchableObject : MonoBehaviour
{
    Renderer rend;
    public Recipient target;
    public GameObject objectToFollow;
    public bool catched;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Follow(Vector3 position)
    {
        transform.position = Vector3.Lerp(transform.position, position, 3 * Time.deltaTime);
    } 


    private void OnTriggerEnter(Collider other)
    {
        if (!catched)
        {
            SelectableObject selectableObject = other.gameObject.GetComponent<SelectableObject>();
            if (selectableObject)
            {
                catched = true;
                selectableObject.AddCatchedObject(this);
            }
        }
        
    }


   

    void OnMouseOver()
    {
        Highlight(target.color);
        target.Highlight(target.color);
        GameManager.Instance.aimed = gameObject;
    }


    void OnMouseExit()
    {
        RemoveHighlight();
        target.RemoveHighlight();
        GameManager.Instance.aimed = null;
    }

    public void Highlight(Color color)
    {
        foreach (Outline outline in transform.GetComponentsInChildren<Outline>())
        {

            outline.OutlineMode = Outline.Mode.OutlineAll;
            outline.OutlineColor = color;
            outline.OutlineWidth = 4f;
        }
        
    }

    public void RemoveHighlight()
    {
        foreach (Outline outline in transform.GetComponentsInChildren<Outline>())
        {

            outline.OutlineMode = Outline.Mode.OutlineHidden;
            outline.OutlineColor = Color.clear;
        }
    }

}
