﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PanoramicCamera : MonoBehaviour
{
    public enum PanCamStatus
    {
        idle,
        startPositioning,
        heightSet,
        newSegment

    }
    public PanCamStatus panCamStatus = PanCamStatus.idle;
    public LineRenderer positioner;
    Vector3 movement;
    public float movingSpeed;
    Camera cam;
    private Vector3 mouseStartPos;
    private Vector3 nextSegmentStart;
    public bool multipleSegments;
    private GameObject positioningCircle;
    public GameObject circlePrefab;
    bool moving = false;

    void Start()
    {
        cam = GetComponent<Camera>();
        positioner = GetComponent<LineRenderer>();
        positioner.positionCount = 2;
    }

    void Update()
    {
        if (moving) transform.Translate(movement * Time.deltaTime);
        multipleSegments = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
        if (GameManager.Instance.selectedObject != null)
        {
            SelectableObject selected = GameManager.Instance.selectedObject.GetComponent<SelectableObject>();
            switch (panCamStatus)
            {

                case PanCamStatus.idle:
                    if (selected.moving)
                    {
                        GameManager.Instance.uIManager.SetHelp("new direction: hold LEFT CLICK\nadd path point: CTRL + hold LEFT CLICK\nselect gift: LEFT CLICK\nselect a Santa: RIGHT CLICK\n");
                    }
                    else
                    {
                        GameManager.Instance.uIManager.SetHelp("free direction: hold LEFT CLICK\nselect gift: LEFT CLICK\nselect a Santa: RIGHT CLICK\n");
                    }

                    break;
                case PanCamStatus.startPositioning:
                    Vector3 firstPoint = (multipleSegments && selected.pathPoints.Count >= 2) ? nextSegmentStart : selected.transform.position;
                    Vector3 perpendicularFoot = FindPerpendicularFoot(firstPoint);
                    if (Input.GetMouseButton(1))
                    {
                        perpendicularFoot = FindPerpendicularFoot(firstPoint);
                        GameManager.Instance.uIManager.SetHelp("select height: lift LEFT CLICK");

                    }
                    if (Input.GetMouseButtonUp(1))
                    {
                        positioner.positionCount = 3;
                        positioner.SetPosition(2, perpendicularFoot);
                        mouseStartPos = Input.mousePosition;

                        selected.path.positionCount = 2;
                        selected.path.SetPosition(0, positioner.GetPosition(0));
                        selected.path.SetPosition(1, GetPathEndPoint());
                        selected.pathPoints.Add(positioner.GetPosition(0));
                        SoundManager.Instance.PlayMenuSound();
                        panCamStatus = PanCamStatus.heightSet;
                    }

                    break;
                case PanCamStatus.heightSet:
                    if (selected.path.positionCount == 0)
                    {
                        CancelPositioning();

                    }
                    else
                    {
                        Vector3 pathEndPoint = GetPathEndPoint();
                        positioner.SetPosition(2, pathEndPoint);

                        selected.path.SetPosition(selected.path.positionCount - 1, pathEndPoint);

                        if (Input.GetMouseButtonDown(0))
                        {
                            selected.path.SetPosition(selected.path.positionCount - 1, positioner.GetPosition(2));
                            selected.pathPoints.Add(positioner.GetPosition(2));
                            Destroy(positioningCircle);
                            selected.GetComponent<SelectableObject>().MoveOnPath();

                            positioner.positionCount = 0;
                            panCamStatus = PanCamStatus.idle;
                            GameManager.Instance.SetStatus(GameManager.Status.pan);
                            SoundManager.Instance.PlayMenuSound();
                        }
                        if (Input.GetMouseButtonDown(1) && multipleSegments)
                        {

                            nextSegmentStart = positioner.GetPosition(2);
                            selected.pathPoints.Add(nextSegmentStart);
                            positioner.SetPosition(0, nextSegmentStart);
                            panCamStatus = PanCamStatus.newSegment;
                            SoundManager.Instance.PlayMenuSound();

                        }
                        GameManager.Instance.uIManager.SetHelp("start moving: RIGHT CLICK\nadd path point: CTRL + hold LEFT CLICK");

                    }
                    break;
                case PanCamStatus.newSegment:
                    Vector3 perpFoot = FindPerpendicularFoot(nextSegmentStart);
                    if (Input.GetMouseButton(1))
                    {
                        GameManager.Instance.uIManager.SetHelp("select height: lift LEFT CLICK");
                        perpFoot = FindPerpendicularFoot(nextSegmentStart);
                    }
                    if (Input.GetMouseButtonUp(1))
                    {

                        SoundManager.Instance.PlayMenuSound();
                        positioner.positionCount = 3;
                        positioner.SetPosition(2, perpFoot);
                        mouseStartPos = Input.mousePosition;
                        panCamStatus = PanCamStatus.heightSet;
                        selected.path.positionCount++;
                        selected.path.SetPosition(selected.path.positionCount - 1, GetPathEndPoint());
                    }
                    break;
            }
        }
        else
        {
            GameManager.Instance.uIManager.SetHelp("select a Santa: RIGHT CLICK\n");
        }
    }



    private Vector3 GetPathEndPoint()
    {
        float distance = Input.mousePosition.y - mouseStartPos.y;

        Vector3 newPos = positioner.GetPosition(1) + new Vector3(0, 0.2f * distance, 0);
        if (newPos.y < 0)
        {
            newPos.y = 0;
        }
        return newPos;
    }

    internal void MoveUp()
    {
        movement = movingSpeed * new Vector3(0, 1, 0);
        moving = true;
    }

    internal void MoveRight()
    {
        movement = movingSpeed * new Vector3(1, 0, 0);
        moving = true;
    }

    internal void MoveLeft()
    {
        movement = movingSpeed * new Vector3(-1, 0, 0);
        moving = true;
    }

    internal void MoveDown()
    {
        movement = movingSpeed * new Vector3(0, -1, 0);
        moving = true;
    }

    internal void StopMoving()
    {
        moving = false;
    }

    private void OnDisable()
    {
        moving = false;
    }

    internal void SetIdle()
    {
        if (panCamStatus != PanCamStatus.idle)
        {
            panCamStatus = PanCamStatus.idle;
            Destroy(positioningCircle);
            positioner.positionCount = 0;
        }
    }

    public void EnterPositioningMode()
    {
        SelectableObject selected = GameManager.Instance.selectedObject.GetComponent<SelectableObject>();
        if (multipleSegments && selected.pathPoints.Count >= 2)
        {
            nextSegmentStart = selected.path.GetPosition(selected.path.positionCount - 1);
            positioner.positionCount = 1;
            positioner.SetPosition(0, nextSegmentStart);
            panCamStatus = PanCamStatus.newSegment;
        }
        else
        {
            selected.pathPoints.Clear();
            selected.StopMoving();
            panCamStatus = PanCamStatus.startPositioning;
            selected.path.positionCount = 1;
        }
        positioningCircle = Instantiate(circlePrefab);
    }

    private Vector3 FindPerpendicularFoot(Vector3 pos)
    {
        positioner.positionCount = 2;
        positioner.SetPosition(0, pos);
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        float cameraAngle = Vector3.Angle(ray.direction, -Vector3.up);
        float heightsDistance = transform.position.y - pos.y;
        float distanceFromObjectPlane = heightsDistance / Mathf.Cos(Mathf.Deg2Rad * cameraAngle);
        Vector3 perpFoot = ray.origin + ray.direction * distanceFromObjectPlane;
        positioner.SetPosition(1, perpFoot);
        positioningCircle.transform.position = pos;
        positioningCircle.transform.localScale = Vector3.Distance(pos, perpFoot) * Vector3.one;
        return perpFoot;

    }

    public void CancelPositioning()
    {
        positioner.positionCount = 0;
        panCamStatus = PanCamStatus.idle;
        GameManager.Instance.SetStatus(GameManager.Status.pan);
    }
}
