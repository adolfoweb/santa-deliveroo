﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Button[] playButton;
    public Text timeLeftText;
    public Text deliveredText;
    public Text deliveredFinalText;
    public GameObject winPanel;
    public GameObject startPanel;
    public Button nextButton;
    public Button retryButton;
    public Text titleText;
    public Text resultText;
    int deliv;
    public Text help;
    // Start is called before the first frame update
    void Start()
    {

        GameManager.Instance.uIManager = this;
        //if (GameManager.Instance.level > 0) titleText.text = "Level " + GameManager.Instance.level;

        nextButton.onClick.AddListener(() =>
        {
            PlayerPrefs.SetInt("LevelReached", GameManager.Instance.level);
            //GameManager.Instance.level++;
            SceneManager.LoadScene("GameView");
            SoundManager.Instance.PlayMenuSound();
            
        });

        retryButton.onClick.AddListener(() =>
        {
            //GameManager.Instance.level++;
            SceneManager.LoadScene("GameView");
            SoundManager.Instance.PlayMenuSound();
        });
    }

    public void OnButtonClick(int i)
    {
        GameManager.Instance.level = i;
        GameManager.Instance.Play();
        startPanel.gameObject.SetActive(false);
        SoundManager.Instance.PlayMenuSound();
    }

    public void DisplayTime(int timeLeft)
    {
        timeLeftText.text = timeLeft.ToString();
    }

    public void DisplayDelivered(int delivered)
    {
        deliv = delivered;
        deliveredText.text = "Delivered: "+delivered + "/" + GameManager.Instance.totalGifts;
        deliveredFinalText.text = "Delivered: " + delivered+"/"+GameManager.Instance.totalGifts;
        
    }


    public void DisplayWinMessage()
    {
        winPanel.SetActive(true);
        if (deliv / (float)GameManager.Instance.totalGifts > GameManager.Instance.winMinPercentage) {
            nextButton.gameObject.SetActive(true);
            resultText.text = "Great Job!";
        }
        else
        {
            resultText.text = "too bad";
            nextButton.gameObject.SetActive(false);
        }
        
        
    }

    public void SetHelp(string message)
    {
        help.text = message;
    }
}
