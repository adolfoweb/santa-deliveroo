﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private float frac;
    private bool isPlaying;
    internal int delivered = 0;
    internal int timeLeft;
    internal UIManager uIManager;
    internal LevelManager levelManager;
    internal SoundManager soundManager;
    private float startTime;
    private Status zoomoutStatus;
    internal int totalGifts;
    public int unblockedLevels = 10;
    public int level = 0;
    public float timeToCompleteFirstLevel;
    public int maxHousesQuantity = 15;
    public int santasQuantity = 10;
    public int befanasQuantity = 10;
    public int treesNumber = 100;
    public int befanaPathPoints = 4;
    public float timeToCompleteIncrementOnNewLevel;
    public int befanasIncrementOnNewLevel = 2;
    public int houseIncrementOnNewLevel = 2;
    public float winMinPercentage = 0.6f;
    public float gameAreaPercentualIncrementOnNewLevel = 0.1f;
    public float speedReductionCarry = 0.1f;
    public float zoomSpeed;
    public float befanasSpeed = 0.8f;
    public Status status = Status.free;
    public GameObject outOfScreen;
    internal GameObject aimed;
    public GameObject panCamUI;
    public Camera freeCamera;
    public Camera panoramicCamera;
    public Camera zoomCamera;
    internal GameObject selectedObject;
    public enum Status
    {
        free,
        pan,
        zoomOut,
        zoomIn,
        positioning,
    }

  

    private static GameManager _instance;
    public static GameManager Instance
    {
        get { return _instance; }
    }


    private void Awake()
    {

            _instance = this;

    }


    // Start is called before the first frame update
    void Start()
    {
        soundManager = GetComponent<SoundManager>();  
    }


    // Update is called once per frame
    void Update()
    {
        if (isPlaying)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                
                SwitchView();
            }
            switch (status)
            {
                case Status.free:

                    break;
                case Status.pan:

                    if (Input.GetMouseButtonDown(0))
                    {
                       SelectObject(aimed);
                    }

                    if (Input.GetMouseButtonDown(1))
                    {
                        if (selectedObject)
                        {

                            CatchableObject catchable = null;
                            if (aimed)
                            {
                                catchable = aimed.GetComponent<CatchableObject>();
                            }
                            if (aimed && catchable)
                            {
                                SelectableObject obj = selectedObject.GetComponent<SelectableObject>();
                                SoundManager.Instance.PlayMenuSound();
                                obj.SetPath(
                                    selectedObject.transform.position,
                                    aimed.transform.position,
                                    panoramicCamera.GetComponent<PanoramicCamera>().multipleSegments & obj.pathPoints.Count >= 2
                                    );
                            }
                            else
                            {
                                panoramicCamera.GetComponent<PanoramicCamera>().EnterPositioningMode();
                                SetStatus(Status.positioning);
                            }
                        }
                    }
                    break;
                case Status.positioning:
                    break;
                case Status.zoomOut:
                    frac += Time.deltaTime * zoomSpeed;
                    if (frac >= 1)
                    {
                        SelectCamera(panoramicCamera);
                        SetStatus(zoomoutStatus);
                    }
                    else
                    {
                        zoomCamera.transform.position = Vector3.Lerp(freeCamera.transform.position, panoramicCamera.transform.position, frac);
                        zoomCamera.transform.rotation = Quaternion.Slerp(freeCamera.transform.rotation, panoramicCamera.transform.rotation, frac);
                        zoomCamera.fieldOfView = Mathf.Lerp(freeCamera.fieldOfView, panoramicCamera.fieldOfView, frac);
                    }
                    uIManager.SetHelp("");
                    break;
                case Status.zoomIn:
                    frac += Time.deltaTime * zoomSpeed;
                    if (frac >= 1)
                    {
                        SelectCamera(freeCamera);
                        SetStatus(Status.free);
                    }
                    else
                    {
                        zoomCamera.transform.position = Vector3.Lerp(panoramicCamera.transform.position, freeCamera.transform.position, frac);
                        zoomCamera.transform.rotation = Quaternion.Slerp(panoramicCamera.transform.rotation, freeCamera.transform.rotation, frac);
                        zoomCamera.fieldOfView = Mathf.Lerp(panoramicCamera.fieldOfView, freeCamera.fieldOfView, frac);
                    }
                    uIManager.SetHelp("");
                    break;
            }
            timeLeft = Mathf.RoundToInt(TimeToCompleteLevel()- (Time.time - startTime));
            if (timeLeft <= 0) EndGame();
            uIManager.DisplayTime(timeLeft);
        }
    }

    public void IncreaseDelivered()
    {

        SoundManager.Instance.PlayDelivered();
        delivered++;
        uIManager.DisplayDelivered(delivered);
        if (delivered >= levelManager.gifts.Count)
        {
            EndGame();
        }
    }

    float TimeToCompleteLevel()
    {
        return timeToCompleteFirstLevel + level * timeToCompleteIncrementOnNewLevel;
    }

    public void EndGame()
    {
        SoundManager.Instance.PlayStartMusic();
        isPlaying = false;
        uIManager.DisplayWinMessage();
        freeCamera.GetComponent<FreeCameraMove>().enabled = false;
        uIManager.SetHelp("");
    }

    public void Play()
    {
        SoundManager.Instance.PlayGameMusic();
        levelManager.CreateLevel();
        
        delivered = 0;
        zoomoutStatus = Status.pan;
        StartCountdown();
        freeCamera.GetComponent<FreeCameraMove>().enabled = true;
        isPlaying = true;
    }

    internal Vector3 CurrentGameArea()
    {
       
       return  new Vector3(
            40 * (1 + level * gameAreaPercentualIncrementOnNewLevel),
            10,
            40 * (1 + level * gameAreaPercentualIncrementOnNewLevel));
    }

    public int CurrentBefanas()
    {
        return befanasQuantity + befanasIncrementOnNewLevel * level;
        
    }
    public int CurrentHouseQuantities()
    {
        return maxHousesQuantity + houseIncrementOnNewLevel * level;
        
    }

    public void SelectObject(GameObject aimed)
    {
        SoundManager.Instance.PlayMenuSound();
        Deselect();
        if (aimed)
        {
            SelectableObject selectable = aimed.GetComponent<SelectableObject>();
            if (selectable)
            {
                selectable.Select();
                selectedObject = aimed;
            }
        }
    }

    public void Deselect()
    {
        if (selectedObject)
        {
            if (status == Status.positioning) CancelPositioning();
            selectedObject.GetComponent<SelectableObject>().Deselect();
            selectedObject = null;
        }

    }

    public void StartCountdown()
    {
        startTime = Time.time;

    }


    private void SwitchView()
    {
        switch (status) {
            case Status.free:
                zoomCamera.transform.position = panoramicCamera.transform.position;
                zoomCamera.transform.rotation = panoramicCamera.transform.rotation;
                frac = 0f;
                SelectCamera(zoomCamera);
                SetStatus(Status.zoomOut);
             break;
            case Status.pan:
            case Status.positioning:
                zoomCamera.transform.rotation = freeCamera.transform.rotation;
                frac = 0f;
                zoomoutStatus = status;
                SelectCamera(zoomCamera);
                SetStatus(Status.zoomIn);
            break;
            case Status.zoomOut:
                SetStatus(Status.zoomIn);
            break;
            case Status.zoomIn:
                SetStatus(Status.zoomOut);
             break;
        }
    }

    private void CancelPositioning()
    {
        SetStatus(Status.pan);
        panoramicCamera.GetComponent<PanoramicCamera>().CancelPositioning();
    }

    private void SelectCamera(Camera selected)
    {
        panCamUI.SetActive(selected == panoramicCamera&isPlaying);
        foreach (Camera c in FindObjectsOfType<Camera>(true))
        {
            if (c == selected)
            {
                c.gameObject.SetActive(true);
            }
            else
            {
                c.gameObject.SetActive(false);
            }
        }

    }

    public void SetStatus(Status s)
    {

        status = s;
    }
}
