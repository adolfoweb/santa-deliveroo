﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeCameraMove : MonoBehaviour
{


	public float movingSpeed = 1;
	public float rotationSpeed = 1;
	public Vector3 prevMousePos;
	
	
	// Use this for initialization
	void OnEnable()
	{
		prevMousePos = Input.mousePosition;

	}

	// Update is called once per frame
	void Update()
	{
		Vector3 rotation = Input.mousePosition - prevMousePos;
		transform.Rotate(-rotation.y *2* rotationSpeed, rotation.x * rotationSpeed, 0);
		transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0);
		Vector3 translation = Input.GetAxis("Vertical") * transform.forward + Input.GetAxis("Horizontal") * transform.right + Input.GetAxis("Height") * Vector3.up;
		translation *= movingSpeed ;
		if (Permitted (translation)) {
			transform.Translate(translation);
			GameManager.Instance.uIManager.SetHelp("forward :W\nleft: A\nbackward: S\nright :D\ndown :Q\nup :E\nview direction: mouse\n tactical view: SPACE");
		}
        else
        {
			GameManager.Instance.uIManager.SetHelp("Move that mouse");
        }
		
		prevMousePos = Input.mousePosition;
	}

	bool Permitted (Vector3 translation)
    {
		bool overGround  = (transform.position + translation).y > 1.6f;

		return overGround; // TODO avoid going inside objects
    }




}