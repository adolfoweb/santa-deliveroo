﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanCameraMove : MonoBehaviour
{
    public HoverButton up, down, left, right;
    public PanoramicCamera panCamera;
    // Start is called before the first frame update
    void Start()
    {
        up.onEnter.AddListener(()=> { panCamera.MoveUp(); });
        down.onEnter.AddListener(() => { panCamera.MoveDown(); });
        right.onEnter.AddListener(() => { panCamera.MoveRight(); });
        left.onEnter.AddListener(() => { panCamera.MoveLeft(); });
        up.onExit.AddListener(() => { panCamera.StopMoving(); });
        down.onExit.AddListener(() => { panCamera.StopMoving(); });
        right.onExit.AddListener(() => { panCamera.StopMoving(); });
        left.onExit.AddListener(() => { panCamera.StopMoving(); });
    }

    private void MoveUp(PointerEventData obj)
    {
        throw new NotImplementedException();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
