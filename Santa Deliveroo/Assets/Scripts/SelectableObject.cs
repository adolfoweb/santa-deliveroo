﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableObject : MonoBehaviour
{

    Renderer[] rend;
    public bool moving;
    float frac;
    public float speed;
    int nextPointIndex;
    public List<Vector3> pathPoints;
    bool selected;
    public LineRenderer path;
    public LineRenderer activePath;
    public List<CatchableObject> catchedObjects;
    private float targetDistance;
    public MovingBot botToFollow;
    public GameObject[] CatchedObjectPlaces;
    bool followingBot;

    void Start()
    {
        rend = FindObjectsOfType<Renderer>();
        catchedObjects = new List<CatchableObject>();
        RemoveHighlight();
    }

    public void MoveOnPath()
    {
        if (!moving)
        {
            frac = 0;
            nextPointIndex = 1;
            moving = true;
            
            transform.LookAt(pathPoints[nextPointIndex]);
            targetDistance = Vector3.Distance(transform.position, pathPoints[nextPointIndex]);
        }
        
    }

    private void Update()
    {
        if (!followingBot)
        {
            if (moving)
            {
                frac += Time.deltaTime * speed / targetDistance;
                if (frac >= 1)
                {
                    frac = 0;
                    nextPointIndex++;
                    if (nextPointIndex == pathPoints.Count)
                    {
                        HideActivePath();
                        StopMoving();
                    }
                    else
                    {
                        targetDistance = Vector3.Distance(transform.position, pathPoints[nextPointIndex]);
                        transform.LookAt(pathPoints[nextPointIndex]);
                        DisplayActivePath(transform.position, pathPoints[nextPointIndex]);
                    }
                }
                else
                {
                    transform.position = Vector3.Lerp(pathPoints[nextPointIndex - 1], pathPoints[nextPointIndex], frac);
                    for (int i = 0; i < catchedObjects.Count; i++)
                    {
                        catchedObjects[i].Follow(CatchedObjectPlaces[ i].transform.position);
                    }
                }
            }
        }
    }

    private void DisplayActivePath(Vector3 pos1, Vector3 pos2)
    {
        activePath.positionCount = 2;
        activePath.SetPosition(0, pos1);
        activePath.SetPosition(1, pos2);
    }

    private void HideActivePath()
    {
        activePath.positionCount = 0;

    }

    public void StopMoving()
    {
        moving = false;
        path.positionCount = 0;
    }

    public void Caught()
    {
        path.positionCount = 0;
        path.enabled = false;
        HideActivePath();
        if (selected)
        {
            
            GameManager.Instance.Deselect();
            Camera panCam = GameManager.Instance.panoramicCamera;
            if (panCam.gameObject.activeSelf) {

                panCam.GetComponent<PanoramicCamera>().SetIdle();
            }
            Deselect();
        }
        foreach(CatchableObject c in catchedObjects)
        {
            c.transform.parent = transform;
        }
        followingBot = true;
    }

    public void SetPath(Vector3 pos1, Vector3 pos2, bool addPoint)
    {

        if (addPoint)
        {
            path.positionCount++;
            path.SetPosition(path.positionCount - 1, pos2);
            pathPoints.Add(pos2);
        }
        else
        {
            StopMoving();

            path.positionCount = 2;
            path.SetPosition(0, pos1);
            path.SetPosition(1, pos2);
            DisplayActivePath(pos1,pos2);
            pathPoints.Clear();
            pathPoints.Add(pos1);
            pathPoints.Add(pos2);
            transform.LookAt(pos2);
            MoveOnPath();
        }
    }
    public void AddCatchedObject(CatchableObject obj)
    {
        if (catchedObjects.Count < 5)
        {
            obj.objectToFollow = gameObject;
            catchedObjects.Add(obj);
            speed *= 1 - GameManager.Instance.speedReductionCarry;
        }
    }

    public void RemoveCatchedObject(CatchableObject obj)
    {
        catchedObjects.Remove(obj);
        speed *= 1 + GameManager.Instance.speedReductionCarry;
    }
    void OnMouseEnter()
    {
        foreach (Renderer r in rend)
        {
            Highlight(Color.yellow);
        }
        GameManager.Instance.aimed = gameObject;
    }


    public void Highlight(Color color)
    {
        foreach (Outline outline in transform.GetComponentsInChildren<Outline>())
        {

            outline.OutlineMode = Outline.Mode.OutlineAll;
            outline.OutlineColor = color;
            outline.OutlineWidth = 4f;
        }
        foreach (CatchableObject obj in catchedObjects)
        {
            obj.Highlight(color);
        }
    }

    void RemoveHighlight()
    {
        foreach (Outline outline in transform.GetComponentsInChildren<Outline>())
        {

            outline.OutlineMode = Outline.Mode.OutlineHidden;
            outline.OutlineColor = Color.clear;
        }
        foreach (CatchableObject obj in catchedObjects)
        {
            obj.RemoveHighlight();
        }
    }


    void OnMouseExit()
    {
        if (!selected)
        {
            RemoveHighlight();
        }
        GameManager.Instance.aimed = null;
    }

    public void Select()
    {
        selected = true;
        Highlight(Color.red);
    }

    public void Deselect()
    {
        selected = false;
        RemoveHighlight();
    }

    public void Moveto(GameObject catchingObject)
    {
        transform.LookAt(catchingObject.transform.position);
        pathPoints=new List<Vector3> {transform.position,catchingObject.transform.position};
        MoveOnPath();
    }

    public void Follow(Vector3 position)
    {
        transform.position = Vector3.Lerp(transform.position, position, 3 * Time.deltaTime);

    }
}