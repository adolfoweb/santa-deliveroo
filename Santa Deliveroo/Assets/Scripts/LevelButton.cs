﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    int levelNumber;
    // Start is called before the first frame update
    void Start()
    {
        levelNumber = int.Parse(GetComponentInChildren<Text>().text);
        GetComponent<Button>().onClick.AddListener(() => OnButtonClick());
        int levReached = PlayerPrefs.GetInt("LevelReached");
        GetComponent<Button>().interactable = (levelNumber<=levReached+1 ) || (levelNumber< GameManager.Instance.unblockedLevels+1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnButtonClick()
    {
        GameManager.Instance.uIManager.OnButtonClick(levelNumber);
        
    }
}
