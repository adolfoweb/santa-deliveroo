﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public Renderer gameArea;
    public Renderer territory;
    public List<Recipient> chimneys;
    public List<CatchableObject> gifts;
    public List<MovingBot> befanas;
    public List<SelectableObject> santas;

    public GameObject giftPrefab;
    public GameObject housePrefab;
    public GameObject santaPrefab;
    public GameObject befanaPrefab;
    public GameObject treePrefab;
    Bounds gameAreaBounds;
    public float chimneyProbability;
    public int houseDistance = 10;
    public int giftDistance = 3;
    public int treeDistance = 5;
    public List<Vector3> landPositions;
    public List<Vector3> skyPositions;
    
    public bool testMode;
    public float befanaFollowingDistance = 5;
    public float instantiationTime;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.levelManager = this;
        


    }

    private void GenerateTrees()
    {
        StartCoroutine(GraduallyGenerateTrees());
        
    }

    IEnumerator GraduallyGenerateTrees()
    {
        for (int i = 0; i < GameManager.Instance.treesNumber; i++)
        {
            yield return new WaitForSeconds(instantiationTime);
            GameObject tree = Instantiate(treePrefab, transform);
            Vector3 treePosition = treeRandomPosition();
            while (treePosition.x > gameAreaBounds.min.x && treePosition.x < gameAreaBounds.max.x &&
                    treePosition.z > gameAreaBounds.min.z && treePosition.z < gameAreaBounds.max.z)
            {
                treePosition = treeRandomPosition();
            }
            tree.transform.position = treePosition;

        }
    }
    private void GenerateSantas()
    {
        StartCoroutine(GraduallyGenerateSantas());
    }

    IEnumerator GraduallyGenerateSantas()
    {

        for (int i = 0; i < GameManager.Instance.santasQuantity; i++)
        {
            yield return new WaitForSeconds(instantiationTime);
            GameObject santa = Instantiate(santaPrefab, transform);
            int randomIndex = Random.Range(0, landPositions.Count - 1);
            santa.transform.position = landPositions[randomIndex];
            landPositions.RemoveAt(randomIndex);
            santa.transform.eulerAngles = new Vector3(0, Random.Range(0, 360), 0) ;
            santas.Add(santa.GetComponent<SelectableObject>());
        }
    }

    void GenerateSanta()
    {
        GameObject santa = Instantiate(santaPrefab, transform);
        
    }

    private void GenerateGifts(Recipient chimney,int index)
    {
            chimney.color = Color.HSVToRGB(index / (float)GameManager.Instance.CurrentHouseQuantities(), 1, 1);
            chimney.GetComponent<Renderer>().material.color = chimney.color;
            int giftsCount = Random.Range(2, 4);
            chimney.numberToCatch = giftsCount;
            for (int j = 0; j < giftsCount; j++)
            {
                GameObject gift = Instantiate(giftPrefab,transform);
                int randomIndex = Random.Range(0, skyPositions.Count - 1);
                gift.transform.position = skyPositions[randomIndex];
                skyPositions.RemoveAt(randomIndex);
                gift.transform.eulerAngles = new Vector3(0, Random.Range(0, 360), 0);
                gift.GetComponentInChildren<Renderer>().material.color = chimney.color;
                gift.GetComponent<CatchableObject>().target = chimney;
                gifts.Add(gift.GetComponent<CatchableObject>());
            }
            GameManager.Instance.totalGifts = gifts.Count;
    }

    private void GenerateBefanas()
    {
        for (int i = 0; i < GameManager.Instance.CurrentBefanas(); i++)
        {
            GameObject befana = Instantiate(befanaPrefab, transform);
            MovingBot movingBot = befana.GetComponent<MovingBot>();
            for (int j = 0; j < GameManager.Instance.befanaPathPoints; j++)
            {
                int randomIndex = Random.Range(0, skyPositions.Count - 1);
                movingBot.pathPoints.Add(skyPositions[randomIndex]);
                skyPositions.RemoveAt(randomIndex);
            }
            befana.transform.position = movingBot.pathPoints[0];
            befanas.Add(movingBot);
        }
    }

    void GenerateBefanas(Stack<Vector3> positions)
    {
        StartCoroutine(GenerateBefanasGradually(positions));
    }


    IEnumerator GenerateBefanasGradually(Stack<Vector3> positions)
    {
        for (int i = 0; i < positions.Count; i++)
        {
            yield return new WaitForSeconds(instantiationTime);
            GameObject befana = Instantiate(befanaPrefab, transform);
            MovingBot movingBot = befana.GetComponent<MovingBot>();
            for (int j = 0; j < GameManager.Instance.befanaPathPoints; j++)
            {
                movingBot.pathPoints.Add(positions.Pop());
                
            }
            befana.transform.position = movingBot.pathPoints[0];
            befanas.Add(movingBot);
        }
    }

    List<Vector3> GetLandPositions()
    {
        List<Vector3> positions = new List<Vector3>();
        Vector3 min = gameAreaBounds.min;
        Vector3 max = gameAreaBounds.max;
        float x = min.x;
        float z = min.z;
        while (x < max.x)
        {
            z = min.z;
            while (z < max.z)
            {
                positions.Add(new Vector3(x, 0, z));
                z += houseDistance;
            }
            x += houseDistance;
        }
        return positions;
    }

    List<Vector3> GetSkyPositions()
    {
        List<Vector3> positions = new List<Vector3>();
        Vector3 min = gameAreaBounds.min;
        Vector3 max = gameAreaBounds.max;
        float x = min.x;
        float y = min.y;
        float z = min.z;
        while (x < max.x)
        {
            y = min.y;
            while (y < max.y)
            {
                z = min.z;
                while (z < max.z)
                {
                    positions.Add(new Vector3(x, y, z));
                    z += giftDistance;
                }
                y += giftDistance;
            }
            x += giftDistance;
        }
        return positions;
    }


    private void GenerateHouses()
    {
        StartCoroutine(GraduallyGenerateHouses());
    }

    IEnumerator GraduallyGenerateHouses()
    {
        for (int j = 0; j < GameManager.Instance.CurrentHouseQuantities(); j++)
        {
            yield return new WaitForSeconds(instantiationTime);
            GameObject house = Instantiate(housePrefab, transform);
            int randomIndex = Random.Range(0, landPositions.Count - 1);
            house.transform.position = landPositions[randomIndex];
            landPositions.RemoveAt(randomIndex);
            house.transform.eulerAngles = new Vector3(0, Random.Range(0, 360), 0);
            
            Recipient chimney = house.GetComponentInChildren<Recipient>();
            if (Random.Range(0, 1) <= chimneyProbability)
            {
                chimney.gameObject.SetActive(true);
                chimneys.Add(chimney);
                GenerateGifts(chimney, chimneys.Count - 1);
            }
            else
            {
                chimney.gameObject.SetActive(false);
            }
        }
    }


    internal void CreateLevel()
    {
        if (testMode)
        {
            GameManager.Instance.maxHousesQuantity = 0;
            GameManager.Instance.santasQuantity = 0;
            GameManager.Instance.befanasQuantity = 1;
            GameManager.Instance.treesNumber = 0;
        }
        gameArea.transform.localScale = GameManager.Instance.CurrentGameArea();
        gameAreaBounds = gameArea.bounds;

        int housesCount = Random.Range(10, GameManager.Instance.CurrentHouseQuantities());
        skyPositions = GetSkyPositions();
        landPositions = GetLandPositions();
        GenerateHouses();
        GenerateSantas();
        GenerateBefanas();
        GenerateTrees();
    }

    Vector3 houseRandomPosition()
    {
        return houseDistance* new Vector3(
                Mathf.RoundToInt(Random.Range(gameAreaBounds.min.x/houseDistance, gameAreaBounds.max.x/ houseDistance)),
                0,
                Mathf.RoundToInt(Random.Range(gameAreaBounds.min.z/ houseDistance, gameAreaBounds.max.z/ houseDistance)));
    }

    Vector3 giftRandomPosition()
    {
        return giftDistance* new Vector3(
                    Mathf.RoundToInt(Random.Range(gameAreaBounds.min.x/giftDistance, gameAreaBounds.max.x / giftDistance)),
                    Mathf.RoundToInt(Random.Range(gameAreaBounds.min.y / giftDistance, gameAreaBounds.max.y / giftDistance)),
                    Mathf.RoundToInt(Random.Range(gameAreaBounds.min.z / giftDistance, gameAreaBounds.max.z / giftDistance)));
    }

    Vector3 treeRandomPosition()
    {
        return treeDistance * new Vector3(
                    Mathf.RoundToInt(Random.Range(territory.bounds.min.x / treeDistance, territory.bounds.max.x / treeDistance)),
                    0,
                    Mathf.RoundToInt(Random.Range(territory.bounds.min.z / treeDistance, territory.bounds.max.z / treeDistance)));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
